Role Name
=========

A role that installs pgAdmin as a service, <https://www.pgadmin.org/>

Role Variables
--------------

The most important variables are listed below:

``` yaml
pgadmin_as_package: True
```

Dependencies
------------

* python3-environment, when installed from the wheel file and not as a distribution package

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
